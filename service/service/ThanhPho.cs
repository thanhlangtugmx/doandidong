//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace service
{
    using System;
    using System.Collections.Generic;
    
    public partial class ThanhPho
    {
        public ThanhPho()
        {
            this.Amthuc = new HashSet<Amthuc>();
            this.DuLich = new HashSet<DuLich>();
            this.LichSu = new HashSet<LichSu>();
            this.VanHoa = new HashSet<VanHoa>();
        }
    
        public int MaTp { get; set; }
        public string TenTp { get; set; }
    
        public virtual ICollection<Amthuc> Amthuc { get; set; }
        public virtual ICollection<DuLich> DuLich { get; set; }
        public virtual ICollection<LichSu> LichSu { get; set; }
        public virtual ICollection<VanHoa> VanHoa { get; set; }
    }
}
