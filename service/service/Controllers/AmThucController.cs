﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace service.Controllers
{
    public class AmThucController : ApiController
    {
        DoAnDiDongEntities1 db = new DoAnDiDongEntities1();
        // GET api/amthuc
        public IEnumerable<Amthuc> Get()
        {
            return db.Amthuc.ToList();
        }

        // GET api/amthuc/5
        public Amthuc Get(int id)
        {
            Amthuc a = new Amthuc();
            a = (from t in db.Amthuc
                where t.MaAmthuc == id
                     select t).FirstOrDefault();

            return a;
        }

        // POST api/amthuc
        public void Post([FromBody]Amthuc value)
        {
            db.Amthuc.Add(value);
            db.SaveChanges();
        }

        // PUT api/amthuc/5
        public void Put(int id, [FromBody]Amthuc value)
        {
            Amthuc temp = db.Amthuc.First(i => i.MaAmthuc == id);
            temp.MaTp = value.MaTp;
            temp.HinhAnh = value.HinhAnh;
            temp.NoiDung = value.NoiDung;
            temp.TenMon = value.TenMon;
            temp.DiaChi = value.DiaChi;

            db.SaveChanges();
        }

        // DELETE api/amthuc/5
        public void Delete(int id)
        {
            Amthuc temp = db.Amthuc.First(i => i.MaAmthuc == id);
            db.Amthuc.Remove(temp);
            db.SaveChanges();
        }
    }
}
