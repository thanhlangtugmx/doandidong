﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Serialization;

namespace service.Controllers
{
    public class LichSuController : ApiController
    {
        // GET api/lichsu
        DoAnDiDongEntities1 db = new DoAnDiDongEntities1();
         ///GET api/amthuc
        public IEnumerable<LichSu> Get()
        {
            return db.LichSu.ToList();
        }

         //GET api/amthuc/5
        public LichSu Get(int id)
        {
            LichSu a = new LichSu();
            a = (from t in db.LichSu
                 where t.MaLichsu == id
                 select t).FirstOrDefault();
            return a;
        }

        // POST api/amthuc
        public void Post([FromBody]LichSu value)
        {
            db.LichSu.Add(value);
            db.SaveChanges();
        }

        // PUT api/amthuc/5
        public void Put(int id, [FromBody]LichSu value)
        {
            LichSu temp = db.LichSu.First(i => i.MaLichsu == id);
            temp.Matp = value.Matp;
            temp.HinhAnh = value.HinhAnh;
            temp.NoiDung = value.NoiDung;
            temp.TenLichSu = value.TenLichSu;
            temp.DiaChi = value.DiaChi;

            db.SaveChanges();
        }

        // DELETE api/amthuc/5
        public void Delete(int id)
        {
            LichSu temp = db.LichSu.First(i => i.MaLichsu == id);
            db.LichSu.Remove(temp);
            db.SaveChanges();
        }
    }
}
