﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace service.Controllers
{
    public class ThanhPhoController : ApiController
    {
        // GET api/thanhpho
        DoAnDiDongEntities1 db = new DoAnDiDongEntities1();
        ///GET api/amthuc
        public IEnumerable<ThanhPho> Get()
        {
            return db.ThanhPho.ToList();
        }

        //GET api/amthuc/5
        public ThanhPho Get(int id)
        {
            ThanhPho a = new ThanhPho();
            a = (from t in db.ThanhPho
                 where t.MaTp == id
                 select t).FirstOrDefault();
            return a;
        }

        // POST api/amthuc
        public void Post([FromBody]ThanhPho value)
        {
            db.ThanhPho.Add(value);
            db.SaveChanges();
        }

        // PUT api/amthuc/5
        public void Put(int id, [FromBody]ThanhPho value)
        {
            ThanhPho temp = db.ThanhPho.First(i => i.MaTp == id);
            temp.MaTp = value.MaTp;
            temp.TenTp = value.TenTp;
           

            db.SaveChanges();
        }

        // DELETE api/amthuc/5
        public void Delete(int id)
        {
            ThanhPho temp = db.ThanhPho.First(i => i.MaTp == id);
            db.ThanhPho.Remove(temp);
            db.SaveChanges();
        }
    }
}
