﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Serialization;
namespace service.Controllers
{
    public class VanHoaController : ApiController
    {
        // GET api/vanhoa
       
        DoAnDiDongEntities1 db = new DoAnDiDongEntities1();
        // GET api/amthuc
        public IEnumerable<VanHoa> Get()
        {
            return db.VanHoa.ToList();
        }

        // GET api/amthuc/5
        public VanHoa Get(int id)
        {
            VanHoa a = new VanHoa();
            a = (from t in db.VanHoa
                 where t.MaVanhoa == id
                 select t).FirstOrDefault();
            return a;
        }

        // POST api/amthuc
        public void Post([FromBody]VanHoa value)
        {
            db.VanHoa.Add(value);
            db.SaveChanges();
        }

        // PUT api/amthuc/5
        public void Put(int id, [FromBody]VanHoa value)
        {
            VanHoa temp = db.VanHoa.First(i => i.MaVanhoa == id);
            temp.MaTp = value.MaTp;
            temp.HinhAnh = value.HinhAnh;
            temp.NoiDung = value.NoiDung;
            temp.TenVanHoa = value.TenVanHoa;
            temp.DiaChi = value.DiaChi;

            db.SaveChanges();
        }

        // DELETE api/amthuc/5
        public void Delete(int id)
        {
            VanHoa temp = db.VanHoa.First(i => i.MaVanhoa == id);
            db.VanHoa.Remove(temp);
            db.SaveChanges();
        }
    }
}
