﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Serialization;
namespace service.Controllers
{
    public class DuLichController : ApiController
    {
        // GET api/dulich
        DoAnDiDongEntities1 db = new DoAnDiDongEntities1();
        // GET api/amthuc
        public IEnumerable<DuLich> Get()
        {
           return db.DuLich.ToList();
        }

        // GET api/amthuc/5
        public DuLich Get(int id)
        {
            DuLich a = new DuLich();
            a = (from t in db.DuLich
                 where t.MaDuLich == id
                 select t).FirstOrDefault();
            return a;
        }

        // POST api/amthuc
        public void Post([FromBody]DuLich value)
        {
            db.DuLich.Add(value);
            db.SaveChanges();
        }

        // PUT api/amthuc/5
        public void Put(int id, [FromBody]DuLich value)
        {
            DuLich temp = db.DuLich.First(i => i.MaDuLich == id);
            temp.MaTp = value.MaTp;
            temp.HinhAnh = value.HinhAnh;
            temp.NoiDung = value.NoiDung;
            temp.TenDiaDiem = value.TenDiaDiem;
            temp.DiaChi = value.DiaChi;

            db.SaveChanges();
        }

        // DELETE api/amthuc/5
        public void Delete(int id)
        {
            DuLich temp = db.DuLich.First(i => i.MaDuLich == id);
            db.DuLich.Remove(temp);
            db.SaveChanges();
        }
    }
}
